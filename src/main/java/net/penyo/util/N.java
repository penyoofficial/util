package net.penyo.util;

/**
 * An object that describes one-to-many relationship.
 *
 * @param <E> the type of elements
 * @author Penyo
 */
public record N<E>(E from, E... to) {

    @SafeVarargs
    public N {
    }

    /**
     * Provides an easier way to construct a {@link N} entity.
     *
     * @param from param that the record needs
     * @param to   param that the record needs
     * @return a {@link N} entity
     */
    @SafeVarargs
    public static <E> N<E> N(E from, E... to) {
        return new N<E>(from, to);
    }
}
