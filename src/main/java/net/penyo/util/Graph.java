package net.penyo.util;

import java.util.*;

/**
 * An object that contains nodes and their edges. A graph cannot contain
 * duplicate nodes; each edge can have two nodes as two ends.
 *
 * @param <E> the type of elements maintained bt this graph
 * @author Penyo
 * @see N
 * @see AbstractGraph
 * @see HashGraph
 */
public interface Graph<E> extends Iterable<E> {

    // Query operations

    /**
     * Returns the number of nodes in this graph. If the graph contains more
     * than {@code Integer.MAX_VALUE} elements, returns {@code Integer
     * .MAX_VALUE}.
     *
     * @return the number of nodes in this graph
     */
    int size();

    /**
     * Returns {@code true} if this graph contains no nodes.
     *
     * @return {@code true} if this graph contains no nodes
     */
    boolean isEmpty();

    /**
     * Returns {@code true} if this graph contains the specified node.
     *
     * @param e element whose presence is this graph is to be tested
     * @return {@code true} if this graph contains the specified node
     * @throws ClassCastException   if the element is of an inappropriate type
     *                              for this graph
     * @throws NullPointerException if the specified element is null and this
     *                              graph does not permit null elements
     */
    boolean contains(E e);

    /**
     * Returns {@code true} if this graph contains the edge between two
     * specified nodes.
     *
     * @param e1 element appointed as one end of the edge
     * @param e2 element appointed as another end of the edge
     * @return {@code true} if this graph contains the edge between two
     * specified nodes
     * @throws ClassCastException   if the element is of an inappropriate type
     *                              for this graph
     * @throws NullPointerException if the specified element is null and this
     *                              graph does not permit null elements
     */
    boolean containsEdge(E e1, E e2);

    /**
     * Returns the edges associated with the node representing the specified
     * element.
     *
     * @param e the element whose associated edges are to be returned
     * @return the edges associated with the node representing the specified
     * element
     * @throws ClassCastException   if the element is of an inappropriate type
     *                              for this graph
     * @throws NullPointerException if the specified element is null and this
     *                              graph does not permit null elements
     */
    Set<E> getEdges(E e);

    /**
     * Returns {@code true} if there is at least one path between two nodes
     * representing the specified elements. If at least one of nodes doesn't
     * exist in this graph, returns {@code false}.
     *
     * @param start node where search begins
     * @param end node where search terminates
     * @return {@code true} if there is at least one path between two nodes
     * representing or {@code false} if at least one of nodes doesn't exist
     * in this graph
     * @throws ClassCastException   if the element is of an inappropriate type
     *                              for this graph
     * @throws NullPointerException if the specified element is null and this
     *                              graph does not permit null elements
     */
    boolean canArrive(E start, E end);

    /**
     * Returns all nodes in the path sequentially from start to end (include
     * these two nodes). If at least one of nodes doesn't exist in this
     * graph, returns empty list.
     *
     * <p>The method is also an implementation of A* algorithm.
     *
     * @param start node where search begins
     * @param end node where search terminates
     * @return all nodes in the path sequentially from start to end or empty
     * list if at least one of nodes doesn't exist in this graph
     * @throws ClassCastException   if the element is of an inappropriate type
     *                              for this graph
     * @throws NullPointerException if the specified element is null and this
     *                              graph does not permit null elements
     */
    List<E> shortestPath(E start, E end);


    // Modification operations

    /**
     * Adds the node representing the specified element to this graph. If the
     * graph previously contained a node for the element, the old node is
     * replaced by the new node.
     *
     * <p>Returns the old element or {@code null} if the graph contained no
     * node for the element.
     *
     * @param e element to be added
     * @return the old element or {@code null}
     * @throws UnsupportedOperationException if the {@code put} operation is
     *                                       not supported by this graph
     * @throws ClassCastException            if the element is of an
     *                                       inappropriate type for this graph
     * @throws NullPointerException          if the specified element is null
     *                                       and this graph does not permit
     *                                       null elements
     */
    boolean put(E e);

    /**
     * Removes the node representing the specified element and associated edges
     * from this graph if the node is present.
     *
     * @param e element to be removed
     * @return if the specified element is removed
     * @throws UnsupportedOperationException if the {@code remove} operation
     *                                       is not supported by this graph
     * @throws ClassCastException            if the element is of an
     *                                       inappropriate type for this graph
     * @throws NullPointerException          if the specified element is null
     *                                       and this graph does not permit
     *                                       null elements
     */
    boolean remove(E e);

    /**
     * Adds the edge associating two nodes representing the specified elements
     * to this graph. If the graph previously contained an edge for the
     * elements, the operation is canceled.
     *
     * @param e1 element appointed as one end of the edge
     * @param e2 element appointed as another end of the edge
     * @throws UnsupportedOperationException if the {@code putEdge} operation
     *                                       is not supported by this graph
     * @throws ClassCastException            if the element is of an
     *                                       inappropriate type for this graph
     * @throws NullPointerException          if the specified element is null
     *                                       and this graph does not permit
     *                                       null elements
     */
    void putEdge(E e1, E e2);

    /**
     * Removes the edge associating two nodes representing the specified
     * elements from this graph if the nodes are both present.
     *
     * @param e1 element appointed as one end of the edge
     * @param e2 element appointed as another end of the edge
     * @throws UnsupportedOperationException if the {@code removeEdge}
     *                                       operation is not supported by
     *                                       this graph
     * @throws ClassCastException            if the element is of an
     *                                       inappropriate type for this graph
     * @throws NullPointerException          if the specified element is null
     *                                       and this graph does not permit
     *                                       null elements
     */
    void removeEdge(E e1, E e2);


    // Bulk operations

    /**
     * Copies all the nodes from the specified graph to this graph, and merge
     * edge set for every nodes if needed. The behavior of this operation is
     * undefined if the specified graph is modified while the operation is in
     * progress.
     *
     * @param g nodes to be stored in this graph
     * @throws UnsupportedOperationException if the {@code putAll} operation
     *                                       is not supported by this graph
     * @throws ClassCastException            if the element is of an
     *                                       inappropriate type for this graph
     * @throws NullPointerException          if the specified element is null
     *                                       and this graph does not permit
     *                                       null elements
     */
    void putAll(Graph<E> g);

    /**
     * Removes all the nodes and edges from this graph. The graph will be empty
     * after this call returns.
     *
     * @throws UnsupportedOperationException if the {@code clear} operation
     *                                       is not supported by this graph
     */
    void clear();


    // Views

    /**
     * Returns a {@link Set} view of elements contained in this graph. The
     * set is backed by the graph, so changes to the graph are reflected in
     * the set, and vice-versa. If the graph is modified while an iteration
     * over the set is in progress (except through the iterator's own {@code
     * remove} operation), the results of the iteration are undefined. The
     * set supports element removal, which removes the corresponding mapping
     * from the graph, via the {@code Iterator.remove}, {@code Collection
     * .remove}, {@code removeAll}, {@code retainAll} and {@code clear}
     * operations. It does not support the {@code add} or {@code addAll}
     * operations.
     *
     * @return a set view of elements contained in this graph
     */
    Set<E> elementSet();


    // Comparison and hashing

    @Override
    boolean equals(Object o);

    @Override
    int hashCode();


    // Defaultable methods

    @Override
    default Iterator<E> iterator() {
        return elementSet().iterator();
    }

    /**
     * Returns an empty unmodifiable graph.
     *
     * @return an empty unmodifiable graph
     */
    @SuppressWarnings("unchecked")
    static <E> Graph<E> of() {
        return (Graph<E>) ImmutableGraph.EMPTY_GRAPH;
    }

    /**
     * Returns an unmodifiable graph containing specified one-to-many
     * relationships.
     *
     * @param n one-to-many relationships
     * @return an unmodifiable graph containing specified one-to-many
     * relationships
     * @throws NullPointerException if at least one of the params is {@code
     *                              null}
     */
    @SafeVarargs
    static <E> Graph<E> of(N<E>... n) {
        return new ImmutableGraph<E>(new HashGraph<>() {{
            for (N<E> n : n) {
                put(n.from());
                for (E to : n.to()) {
                    put(to);
                    putEdge(n.from(), to);
                }
            }
        }});
    }

    /**
     * Returns an unmodifiable graph containing the nodes of the given graph.
     * The given graph must not be null, and it must not contain any null
     * nodes. If the given graph is subsequently modified, the returned graph
     * will not reflect such modifications.
     *
     * @param g a graph from which nodes are drawn, must be non-null
     * @return an unmodifiable graph containing the nodes of the given graph
     * @throws NullPointerException if graph is null, or if it contains any
     *                              null nodes
     */
    static <E> Graph<E> copyOf(Graph<E> g) {
        return new ImmutableGraph<>(g);
    }
}
