package net.penyo.util;

/**
 * A directed graph where edges have a direction, going from one node to
 * another. This interface extends the {@link Graph} interface and retains
 * its properties, but all edges are directed.
 *
 * @param <E> the type of elements maintained by this graph
 * @author Penyo
 * @see HashDirectedGraph
 */
public interface DirectedGraph<E> extends Graph<E> {
}
