package net.penyo.util.cypher;

/**
 * A standard of security level. If there isn't special introduction, {@code
 * B256} can mean key has a length of 256 bits.
 *
 * @author Penyo
 */
public enum SecurityLevel {

    B256(256), B512(512), B1024(1024), B2048(2048);

    public final int value;

    SecurityLevel(int value) {
        this.value = value;
    }
}
