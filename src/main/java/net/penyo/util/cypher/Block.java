package net.penyo.util.cypher;

import java.util.function.Function;

/**
 * Represents a block in a blockchain, storing the hash of the previous block,
 * the data, and the hash of the data.
 *
 * <p>This record class defines a block structure essential for maintaining the
 * integrity and sequence of blockchain transactions. It ensures that each block
 * is linked to the previous one through a cryptographic hash, and also contains
 * the data and its hash.
 *
 * <p>The {@code hashOfPreviousBlock} field denotes the hash of the previous
 * block in the blockchain, ensuring the continuity and immutability of the
 * chain. The {@code data} field contains the actual data of the block, and
 * {@code hashOfData} represents the hash of this data.
 *
 * <p>Example usage:
 * <pre>{@code
 * Block block = new Block("previousBlockHash", dataObject);
 * }</pre>
 *
 * <p>This class is immutable and thread-safe.
 *
 * @param hashOfPreviousBlock the hash of the previous block
 * @param data                the data contained in the block
 * @author Penyo
 */
public record Block(String hashOfPreviousBlock, Object data) {

    public static Function<String, Boolean> hashRule = h -> h.startsWith("ac233");

    /**
     * Computes hash of the block data and returns it as a hexadecimal string.
     *
     * @return the hexadecimal hash of the block data
     */
    public String hash() {
        return Digester.sample.dirtyHash(data.toString(), hashRule, (h, f) -> f + h);
    }
}
