package net.penyo.util.cypher;

import net.penyo.util.annotation.Incubating;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * The {@code SHA} class implements the {@code Digester} interface and
 * provides methods for hashing strings using the SHA algorithm.
 *
 * @author Penyo
 */
@Incubating
public class SHA implements Digester {

    private final MessageDigest digest;

    public SHA(SecurityLevel sl) {
        try {
            digest = MessageDigest.getInstance("SHA-" + sl.value);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String hash(String o) {
        return dirtyHash(o, h -> true, (h, f) -> h);
    }

    @Override
    public String dirtyHash(String o, Function<String, Boolean> hashRule, BiFunction<String, Object, String> resolver) {
        String hash = "";

        int factor = 0;
        for (String oCopy = o; !hashRule.apply(hash); oCopy = resolver.apply(o, factor++)) {
            StringBuilder hexString = new StringBuilder();
            for (byte b : digest.digest(oCopy.getBytes())) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            hash = hexString.toString();
        }
        return hash;
    }
}
