package net.penyo.util.cypher;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code RSA} class provides encryption and decryption methods based on
 * the RSA algorithm. This class implements the {@code Encryptor} interface
 * and uses {@code BigInteger} for mathematical operations.
 *
 * <p>At the beginning, the initializer will create 2 prime numbers as {@code p}
 * and {@code q}, and calculate their product as {@code N}. Then a number
 * {@code e} is chosen such that it is relatively prime to {@code φ(N)} where
 * {@code φ(N)} is the Euler's totient function of {@code N}. The multiplicative
 * inverse of {@code e} modulo {@code φ(N)} is calculated as {@code d}, which
 * is the private key exponent.
 *
 * <p>Encryption and decryption are performed using the following formulas:
 * <ul>
 *   <li>Encryption: {@code c = m^e mod N}</li>
 *   <li>Decryption: {@code m = c^d mod N}</li>
 * </ul>
 *
 * <p>Where:
 * <ul>
 *   <li>{@code m} is the plaintext message represented as a BigInteger</li>
 *   <li>{@code c} is the ciphertext represented as a BigInteger</li>
 *   <li>{@code e} is the public key exponent</li>
 *   <li>{@code d} is the private key exponent</li>
 *   <li>{@code N} is the modulus for both the public and private keys</li>
 * </ul>
 *
 * <p>Example usage:
 * <pre>{@code
 * RSA rsa = new RSA(SecurityLevel.B2048);
 * System.out.println("PublicKeys (N, e): " + rsa.publicKeys);
 * System.out.println("PrivateKeys (N, d): " + rsa.privateKeys);
 *
 * String message = Object.toString();
 * String encrypted = rsa.encrypt(message);
 * String decrypted = rsa.decrypt(encrypted, rsa.privateKeys);
 * System.out.println("Original: " + message);
 * System.out.println("Encrypted: " + encrypted);
 * System.out.println("Decrypted: " + decrypted);
 * }</pre>
 *
 * @author Penyo
 */
public class RSA implements Encryptor {

    public final List<String> publicKeys = new ArrayList<>();

    public final List<String> privateKeys = new ArrayList<>();

    public RSA(SecurityLevel sl) {
        int slv = sl.value;

        BigInteger p = Number.getRandomPrime(Number.TWO.pow(slv - 1), Number.TWO.pow(slv));
        BigInteger q = Number.getRandomPrime(Number.TWO.pow(slv - 1), Number.TWO.pow(slv));
        BigInteger n = p.multiply(q);

        BigInteger r = p.subtract(Number.ONE).multiply(q.subtract(Number.ONE));

        BigInteger e = Number.of(65537);
        while (!Number.isCoPrime(e, r))
            e = Number.getRandomPrime(Number.TWO, r);
        BigInteger d = e.modInverse(r);

        publicKeys.add(n.toString());
        publicKeys.add(e.toString());
        privateKeys.add(n.toString());
        privateKeys.add(d.toString());
    }

    @Override
    public String encrypt(String original) {
        return Number.of(Encryptor.toFlow(original)).modPow(Number.of(publicKeys.getLast()), Number.of(publicKeys.getFirst())).toString();
    }

    @Override
    @SuppressWarnings("unchecked")
    public String decrypt(String cyphered, Object key) {
        List<String> keyPair = (List<String>) key;
        return Encryptor.fromFlow(Number.of(cyphered).modPow(Number.of(keyPair.getLast()), Number.of(keyPair.getFirst())).toString());
    }
}
