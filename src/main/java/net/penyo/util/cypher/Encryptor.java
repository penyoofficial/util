package net.penyo.util.cypher;

import java.nio.charset.StandardCharsets;

/**
 * The {@code Encryptor} interface provides methods for encrypting,
 * decrypting, and signing strings. It also includes utility methods for
 * converting strings to and from Unicode byte arrays.
 *
 * @author Penyo
 */
public interface Encryptor {

    /**
     * Encrypts the given original string.
     *
     * @param original the original string to be encrypted.
     * @return the encrypted string.
     */
    String encrypt(String original);

    /**
     * Decrypts the given encrypted string using the provided key.
     *
     * @param cyphered the encrypted string to be decrypted.
     * @param key      the key used for decryption.
     * @return the decrypted string.
     */
    String decrypt(String cyphered, Object key);

    /**
     * Signs the given original string using hash function. The string is
     * first hashed the hash function and then encrypted.
     *
     * @param original the original string to be signed.
     * @return the signed string.
     */
    default String sign(String original) {
        return encrypt(Digester.sample.hash(original));
    }

    int bpb = 3;
    int bOffset = 128;

    /**
     * Converts the given string to a custom flow format.
     *
     * @param original the original string to be converted.
     * @return the flow format representation of the string.
     */
    static String toFlow(String original) {
        StringBuilder cBytes = new StringBuilder();
        for (byte c : original.getBytes(StandardCharsets.UTF_16))
            cBytes.append(String.format("%0" + bpb + "d", c + bOffset));

        return cBytes.toString();
    }

    /**
     * Converts the given flow format string back to a regular string.
     *
     * @param flow the flow format string to be converted.
     * @return the regular string representation of the flow format.
     */
    static String fromFlow(String flow) {
        StringBuilder flowBuilder = new StringBuilder(flow);
        while (flowBuilder.length() % bpb != 0) flowBuilder.insert(0, "0");
        flow = flowBuilder.toString();
        byte[] result = new byte[flow.length() / bpb];
        for (int i = 0; i < flow.length(); i += bpb)
            result[(i + 1) / bpb] = (byte) (Integer.parseInt(flowBuilder.substring(i, i + bpb)) - bOffset);

        return new String(result, StandardCharsets.UTF_16);
    }
}
