package net.penyo.util.cypher;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Represents a basic blockchain structure composed of blocks, each
 * containing data and linked cryptographically to the previous block.
 *
 * <p>A blockchain maintains integrity and sequence of data through hashing.
 * Each block in the chain references the hash of the previous block,
 * ensuring immutability and continuity of the chain.
 *
 * <p>Example usage:
 * <pre>{@code
 * Blockchain blockchain = new Blockchain();
 * blockchain.addBlock("First block data");
 * blockchain.addBlock("Second block data");
 *
 * System.out.println("Blockchain:");
 * System.out.println(blockchain);
 *
 * System.out.println("Blockchain is valid: " + blockchain.isValid());
 * }</pre>
 *
 * <p>This class provides basic blockchain functionality with methods to add
 * blocks, validate the blockchain, iterate through blocks, and display the
 * blockchain content.
 *
 * @author Penyo
 */
public class Blockchain implements Iterable<Block> {

    private final LinkedList<Block> blocks;

    /**
     * Constructs a new blockchain with an initial block containing
     * predefined data.
     */
    public Blockchain() {
        blocks = new LinkedList<>() {{
            add(new Block("", "Hello, world!"));
        }};
    }

    /**
     * Adds a new block with the specified data to the blockchain.
     *
     * @param data the data to be added to the new block
     */
    public void addBlock(Object data) {
        blocks.add(new Block(blocks.getLast().hash(), data));
    }

    /**
     * Checks if the blockchain is valid by verifying the integrity of each
     * block and ensuring all blocks are linked correctly.
     *
     * @return {@code true} if the blockchain is valid, {@code false} otherwise
     */
    public boolean isValid() {
        for (int i = 1; i < blocks.size() - 1; i++) {
            Block current = blocks.get(i);
            Block next = blocks.get(i + 1);
            if (!next.hashOfPreviousBlock().equals(current.hash()) || !Block.hashRule.apply(current.hash()))
                return false;
        }
        return true;
    }

    @Override
    public Iterator<Block> iterator() {
        return blocks.iterator();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Block block : blocks) {
            sb.append(block.hash()).append("\t|\t").append(block.data()).append("\n");
        }
        return sb.toString();
    }
}
