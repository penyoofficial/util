package net.penyo.util.cypher;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * The {@code Digester} interface provides methods for hashing strings and
 * for generating "dirty" hashes using custom rules and resolvers.
 *
 * @author Penyo
 */
public interface Digester {

    Digester sample = new SHA(SecurityLevel.B256);

    /**
     * Hashes the given string.
     *
     * @param o the string to be hashed.
     * @return the hashed string.
     */
    String hash(String o);

    /**
     * Generates a "dirty" hash of the given string using a custom hash rule
     * and resolver.
     *
     * @param o        the string to be hashed.
     * @param hashRule the hash rule function that determines if the string
     *                 needs to be "dirty" hashed.
     * @param resolver the resolver function that generates the "dirty" hash.
     * @return the "dirty" hashed string.
     */
    String dirtyHash(String o, Function<String, Boolean> hashRule, BiFunction<String, Object, String> resolver);
}
