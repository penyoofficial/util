package net.penyo.util.cypher;

import net.penyo.util.annotation.Incubating;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * The {@code Number} class provides utility methods for working with {@code
 * BigInteger} instances. This includes methods for checking primality,
 * generating random numbers and primes, and creating {@code BigInteger}
 * instances.
 *
 * @author Penyo
 */
@Incubating
public class Number {

    public static final BigInteger ONE = BigInteger.ONE;
    public static final BigInteger TWO = BigInteger.TWO;

    /**
     * Checks if the given {@code BigInteger} is a probable prime.
     *
     * @param n the {@code BigInteger} to be checked.
     * @return {@code true} if the number is a probable prime, {@code false} otherwise.
     */
    public static boolean isPrime(BigInteger n) {
        return n.isProbablePrime(40);
    }

    /**
     * Checks if the two given {@code BigInteger} values are coprime.
     *
     * @param n1 the first {@code BigInteger}.
     * @param n2 the second {@code BigInteger}.
     * @return {@code true} if the numbers are coprime, {@code false} otherwise.
     */
    public static boolean isCoPrime(BigInteger n1, BigInteger n2) {
        return n1.gcd(n2).equals(ONE);
    }

    /**
     * Generates a random {@code BigInteger} within the specified range.
     *
     * @param min the minimum value (inclusive).
     * @param max the maximum value (exclusive).
     * @return a random {@code BigInteger} between {@code min} (inclusive) and {@code max} (exclusive).
     */
    public static BigInteger getRandom(BigInteger min, BigInteger max) {
        if (max.compareTo(min) <= 0) return min;
        BigInteger range = max.subtract(min);
        BigInteger randomBigIntInRange;
        do {
            randomBigIntInRange = new BigInteger(range.bitLength(), new SecureRandom());
        } while (randomBigIntInRange.compareTo(range) >= 0);
        return randomBigIntInRange.add(min);
    }

    /**
     * Generates a random prime {@code BigInteger} within the specified range.
     *
     * @param min the minimum value (inclusive).
     * @param max the maximum value (exclusive).
     * @return a random prime {@code BigInteger} between {@code min} (inclusive) and {@code max} (exclusive).
     * @throws RuntimeException if a prime number cannot be found within the specified range.
     */
    public static BigInteger getRandomPrime(BigInteger min, BigInteger max) {
        if (min.compareTo(TWO) < 0) return TWO;
        for (BigInteger i = getRandom(min, max); i.compareTo(min) > 0; i = i.subtract(ONE))
            if (isPrime(i)) return i;
        throw new RuntimeException("Can't get random prime number between " + min + " and " + max + "!");
    }

    /**
     * Converts an integer to a {@code BigInteger}.
     *
     * @param n the integer to be converted.
     * @return the {@code BigInteger} representation of the integer.
     */
    public static BigInteger of(int n) {
        return BigInteger.valueOf(n);
    }

    /**
     * Converts a string to a {@code BigInteger}.
     *
     * @param n the string to be converted.
     * @return the {@code BigInteger} representation of the string.
     */
    public static BigInteger of(String n) {
        return new BigInteger(n);
    }
}
