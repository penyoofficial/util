package net.penyo.util;

import java.util.*;

/**
 * This class provides a skeletal implementation of the {@link Graph}
 * interface, to minimize the effort required to implement this interface.
 *
 * @param <E> the type of elements maintained bt this graph
 * @author Penyo
 * @see HashGraph
 */
public abstract class AbstractGraph<E> implements Graph<E> {

    protected Map<E, Set<E>> nodes;

    /**
     * Constructs an empty {@code AbstractGraph}.
     * The resulting graph contains no nodes or edges.
     */
    protected AbstractGraph() {
    }

    @Override
    public int size() {
        return elementSet().size();
    }

    @Override
    public boolean isEmpty() {
        return elementSet().isEmpty();
    }

    @Override
    public boolean contains(E e) {
        return elementSet().contains(e);
    }

    @Override
    public boolean containsEdge(E e1, E e2) {
        if (!contains(e1) || !contains(e2)) return false;
        return nodes.get(e1).contains(e2);
    }

    @Override
    public Set<E> getEdges(E e) {
        if (!contains(e)) return Set.of();
        return nodes.get(e);
    }

    @Override
    public boolean canArrive(E start, E end) {
        return !shortestPath(start, end).isEmpty();
    }

    @Override
    public List<E> shortestPath(E start, E end) {
        if (!contains(start) || !contains(end)) return List.of();

        Map<E, Double> gScore = new HashMap<>();
        Map<E, Double> fScore = new HashMap<>();
        Map<E, E> cameFrom = new HashMap<>();
        PriorityQueue<E> openSet = new PriorityQueue<>(Comparator.comparingDouble(fScore::get));

        for (E node : elementSet()) {
            gScore.put(node, Double.MAX_VALUE);
            fScore.put(node, Double.MAX_VALUE);
        }
        gScore.put(start, 0.0);
        fScore.put(start, 0.0);

        openSet.offer(start);

        while (!openSet.isEmpty()) {
            E current = openSet.poll();
            if (current.equals(end)) break;

            for (E neighbor : getEdges(current)) {
                double tentativeGScore = gScore.get(current) + 1;
                if (tentativeGScore < gScore.get(neighbor)) {
                    cameFrom.put(neighbor, current);
                    gScore.put(neighbor, tentativeGScore);
                    fScore.put(neighbor, tentativeGScore);
                    if (!openSet.contains(neighbor)) {
                        openSet.offer(neighbor);
                    }
                }
            }
        }

        List<E> shortestPath = new LinkedList<>();
        E currentNode = end;
        while (currentNode != null) {
            shortestPath.add(0, currentNode);
            currentNode = cameFrom.get(currentNode);
        }

        return shortestPath;
    }

    @Override
    public boolean put(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(E e) {
        if (elementSet().contains(e)) for (E ee : elementSet())
            removeEdge(ee, e);
        return elementSet().remove(e);
    }

    @Override
    public void putEdge(E e1, E e2) {
        if (!contains(e1) || !contains(e2)) return;
        nodes.get(e1).add(e2);
        nodes.get(e2).add(e1);
    }

    @Override
    public void removeEdge(E e1, E e2) {
        if (!contains(e1) || !contains(e2)) return;
        nodes.get(e1).remove(e2);
        nodes.get(e2).remove(e1);
    }

    @Override
    public void putAll(Graph<E> g) {
        for (E e : g.elementSet()) {
            put(e);
            nodes.get(e).addAll(g.getEdges(e));
        }
    }

    @Override
    public void clear() {
        nodes.clear();
    }

    @Override
    public Set<E> elementSet() {
        return nodes.keySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractGraph<?> that = (AbstractGraph<?>) o;
        return Objects.equals(nodes, that.nodes);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(nodes);
    }

    @Override
    public String toString() {
        return nodes.toString();
    }
}
