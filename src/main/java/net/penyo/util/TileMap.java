package net.penyo.util;

import java.util.*;

/**
 * An object representing a tile map consisting of tiles. Each tile in the
 * map can store a value of type E and has coordinates (x, y).
 *
 * @param <E> the type of elements maintained by this tile map
 * @author Penyo
 * @see Tile
 */
public interface TileMap<E> {

    /**
     * An object representing a tile in the tile map with coordinates (x, y)
     * and a value.
     *
     * @param <E> the type of the value stored in the tile
     */
    interface Tile<E> {

        /**
         * Gets the x-coordinate of the tile.
         *
         * @return the x-coordinate of the tile
         */
        int x();

        /**
         * Gets the y-coordinate of the tile.
         *
         * @return the y-coordinate of the tile
         */
        int y();

        /**
         * Gets the value of the tile.
         *
         * @return the value of the tile
         */
        E value();
    }

    /**
     * Returns the number of tiles in this tile map.
     *
     * @return the number of tiles in this tile map
     */
    int size();

    /**
     * Checks if a tile with the specified coordinates exists in the tile map.
     *
     * @param x the x-coordinate of the tile
     * @param y the y-coordinate of the tile
     * @return {@code true} if the tile exists
     */
    boolean contains(int x, int y);

    /**
     * Gets the value of the tile at the specified coordinates.
     *
     * @param x the x-coordinate of the tile
     * @param y the y-coordinate of the tile
     * @return the value of the tile
     */
    Tile<E> get(int x, int y);

    /**
     * Gets the neighbors of the tile at the specified coordinates in the
     * tile map.
     *
     * @param x the x-coordinate of the tile
     * @param y the y-coordinate of the tile
     * @return a set of neighboring tiles
     */
    Set<Tile<E>> getNeighbors(int x, int y);

    /**
     * Checks if the tile at the specified coordinates is isolated (impassable).
     *
     * @param x the x-coordinate of the tile
     * @param y the y-coordinate of the tile
     * @return {@code true} if the tile is isolated
     */
    boolean isIsolated(int x, int y);

    /**
     * Checks if it is possible to arrive at the ending tile from the
     * starting tile in the tile map.
     *
     * @param x1 the x-coordinate of the starting tile
     * @param y1 the y-coordinate of the starting tile
     * @param x2 the x-coordinate of the ending tile
     * @param y2 the y-coordinate of the ending tile
     * @return {@code true} if it is possible to arrive at the ending tile
     * from the starting tile
     */
    boolean canArrive(int x1, int y1, int x2, int y2);

    /**
     * Finds the shortest path between two tiles represented by their
     * coordinates in the tile map.
     *
     * @param x1 the x-coordinate of the starting tile
     * @param y1 the y-coordinate of the starting tile
     * @param x2 the x-coordinate of the ending tile
     * @param y2 the y-coordinate of the ending tile
     * @return a list of tiles representing the shortest path from the
     * starting tile to the ending tile, inclusive
     */
    List<Tile<E>> shortestPath(int x1, int y1, int x2, int y2);

    /**
     * Sets the isolation state of the tile at the specified coordinates. If
     * {@code value} is {@code true}, the tile is set to be isolated
     * (impassable) and its connections to neighboring tiles are removed. If
     * {@code value} is {@code false}, the tile is made accessible (passable)
     * and connections to its neighboring tiles are restored.
     *
     * @param x     the x-coordinate of the tile
     * @param y     the y-coordinate of the tile
     * @param value {@code true} to set the tile as isolated, {@code false}
     *              to set the tile as accessible
     */
    void setIsolated(int x, int y, boolean value);
}
