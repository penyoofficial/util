package net.penyo.util;

/**
 * A hash-based implementation of a directed graph. This class extends
 * {@link HashGraph} to provide a directed graph where edges have a direction
 * from one node to another.
 *
 * @param <E> the type of elements maintained by this graph
 * @author Penyo
 */
public class HashDirectedGraph<E> extends HashGraph<E> implements DirectedGraph<E> {

    @Override
    public void putEdge(E e1, E e2) {
        if (!contains(e1) || !contains(e2)) return;
        nodes.get(e1).add(e2);
    }

    @Override
    public void removeEdge(E e1, E e2) {
        if (!contains(e1) || !contains(e2)) return;
        nodes.get(e1).remove(e2);
    }
}
