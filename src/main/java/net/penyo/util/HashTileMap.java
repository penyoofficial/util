package net.penyo.util;

import java.util.*;
import java.util.function.Function;


/**
 * Represents a grid of tiles, implemented using a hash-based structure. Each
 * tile in the grid can hold a value of type {@code E}. The grid allows
 * setting tiles as isolated (impassable) or accessible (passable), and
 * supports finding neighbors and shortest paths.
 *
 * @param <E> the type of elements held by the tiles
 * @author Penyo
 * @see Tile
 */
public class HashTileMap<E> implements TileMap<E> {

    /**
     * An implementation of {@link Tile}.
     */
    private record TileImpl<E>(int x, int y, E value) implements Tile<E> {

        @Override
        public String toString() {
            return "[" + x + ", " + y + "]" + (value != null ? "(" + value + ")" : "");
        }
    }

    private final Graph<Tile<E>> g;

    private final boolean allowWalkingDiagonally;

    /**
     * Constructs a HashTileMap with the specified dimensions and value mapper.
     *
     * @param xLen                   the width of the grid
     * @param yLen                   the height of the grid
     * @param allowWalkingDiagonally if diagonal walking is allowed
     * @param valueMapper            a function to map coordinates to tile values
     */
    public HashTileMap(int xLen, int yLen, boolean allowWalkingDiagonally, Function<Integer, E> valueMapper) {
        g = new HashGraph<>();
        for (int y = 0; y < yLen; y++)
            for (int x = 0; x < xLen; x++)
                g.put(new TileImpl<>(x, y, valueMapper.apply(y * yLen + x)));
        this.allowWalkingDiagonally = allowWalkingDiagonally;
        for (Tile<E> b : g)
            setIsolated(b.x(), b.y(), false);
    }

    @Override
    public int size() {
        return g.size();
    }

    @Override
    public boolean contains(int x, int y) {
        return get(x, y) != null;
    }

    @Override
    public Tile<E> get(int x, int y) {
        for (Tile<E> b : g)
            if (b.x() == x && b.y() == y) return b;
        return null;
    }

    @Override
    public Set<Tile<E>> getNeighbors(int x, int y) {
        if (!contains(x, y)) return Set.of();
        return new LinkedHashSet<>() {{
            if (HashTileMap.this.contains(x - 1, y)) add(get(x - 1, y));
            if (HashTileMap.this.contains(x + 1, y)) add(get(x + 1, y));
            if (HashTileMap.this.contains(x, y - 1)) add(get(x, y - 1));
            if (HashTileMap.this.contains(x, y + 1)) add(get(x, y + 1));
            if (allowWalkingDiagonally) {
                if (HashTileMap.this.contains(x - 1, y - 1))
                    add(get(x - 1, y - 1));
                if (HashTileMap.this.contains(x + 1, y - 1))
                    add(get(x + 1, y - 1));
                if (HashTileMap.this.contains(x - 1, y + 1))
                    add(get(x - 1, y + 1));
                if (HashTileMap.this.contains(x + 1, y + 1))
                    add(get(x + 1, y + 1));
            }
        }};
    }

    @Override
    public boolean isIsolated(int x, int y) {
        if (!contains(x, y)) return true;
        boolean flag = true;
        for (Tile<E> b : getNeighbors(x, y))
            if (g.containsEdge(get(x, y), b)) flag = false;
        return flag;
    }

    @Override
    public boolean canArrive(int x1, int y1, int x2, int y2) {
        return g.canArrive(get(x1, y1), get(x2, y2));
    }

    @Override
    public List<Tile<E>> shortestPath(int x1, int y1, int x2, int y2) {
        return g.shortestPath(get(x1, y1), get(x2, y2));
    }

    @Override
    public void setIsolated(int x, int y, boolean value) {
        if (!contains(x, y)) return;
        for (Tile<E> b : getNeighbors(x, y))
            if (value) {
                g.removeEdge(get(x, y), b);
                if (allowWalkingDiagonally && isIsolated(x, y) && (Math.abs(x - b.x()) + Math.abs(y - b.y()) == 2))
                    g.removeEdge(get(x, b.y()), get(b.x(), y));
            } else g.putEdge(get(x, y), b);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashTileMap<?> that = (HashTileMap<?>) o;
        return Objects.equals(g, that.g);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(g);
    }

    @Override
    public String toString() {
        return g.toString();
    }
}
