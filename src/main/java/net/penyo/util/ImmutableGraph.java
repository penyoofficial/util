package net.penyo.util;

import java.util.*;

/**
 * Represents an immutable graph structure where nodes and edges cannot be
 * modified after the graph is created. This ensures that the graph remains
 * constant and thread-safe.
 *
 * <p>Not part of the public API.
 *
 * @param <E> the type of elements maintained bt this graph
 * @author Penyo
 */
class ImmutableGraph<E> extends AbstractGraph<E> {

    protected static final Graph<Object> EMPTY_GRAPH = new ImmutableGraph<>();

    /**
     * Constructs an empty {@code ImmutableGraph}.
     * The resulting graph contains no nodes or edges.
     */
    protected ImmutableGraph() {
        nodes = Map.of();
    }

    /**
     * Constructs an {@code ImmutableGraph} based on an existing graph. The
     * nodes and edges from the specified graph are copied into the new
     * immutable graph.
     *
     * @param g the existing graph from which to create the immutable graph
     */
    protected ImmutableGraph(Graph<E> g) {
        nodes = Collections.unmodifiableMap(((AbstractGraph<E>) g).nodes);
    }

    @Override
    public boolean put(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putEdge(E e1, E e2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeEdge(E e1, E e2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Graph<E> g) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }
}
