package net.penyo.util;

import java.util.*;

/**
 * Hash graph based implementation of the {@link Graph} interface. This
 * implementation provides all the optional graph operations, and permits
 * {@code null} nodes.
 *
 * @param <E> the type of elements maintained bt this graph
 * @author Penyo
 */
public class HashGraph<E> extends AbstractGraph<E> {

    /**
     * Constructs an empty {@code HashGraph} based on {@code HashMap}.
     * The resulting graph contains no nodes or edges.
     */
    public HashGraph() {
        nodes = new HashMap<>();
    }

    @Override
    public boolean put(E e) {
        if (contains(e)) return false;
        return nodes.put(e, new HashSet<>()) == null;
    }
}
